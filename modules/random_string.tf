terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  
}

resource "random_string" "random" {
  length           = 16
  special          = true
  override_special = "/@£$"
}

output "random_name" {
  value = random_string.random.result
}
